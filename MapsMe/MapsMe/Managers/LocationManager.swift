//
//  LocationManager.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/26/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import Foundation
import CoreLocation



protocol LocationManagerDelegate: class {
    func didFindLocation(_ location: CLLocation)
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    
    weak var delegate: LocationManagerDelegate?
    
    
    
    var manager = CLLocationManager()
    
    
    private override init() {
        super.init()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            self.delegate?.didFindLocation(location)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("odobreno")
            self.manager.startUpdatingLocation()
            
        }else {
            print("nije odobreno")
        }
    }
}


