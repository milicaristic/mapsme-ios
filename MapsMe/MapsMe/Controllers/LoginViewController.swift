//
//  LoginViewController.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/15/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextFild: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    private let dataService: DataService = FirebaseService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpElements()
    }
    
    func setUpElements() {
        
        //Hide error label
        errorLabel.alpha = 0
        
        //Style the elements
        Utilities.styleTextFiled(emailTextField)
        Utilities.styleTextFiled(passwordTextFild)
        Utilities.styleFilledButton(loginButton)
        
        
        
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextFild.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        dataService.login(email: email, password: password) { (error) in
            
            self.dataService.getUser { (user, error) in
                if error != nil {
                    //show error
                    return
                }
                UserManager.shared.currentUser = user
                
                DispatchQueue.main.async {[weak self] in
                    if (error != nil ){
                        self?.showError(error!.localizedDescription)
                    }else{
                        let mapViewController =  self?.storyboard?.instantiateViewController(withIdentifier: "mainNav" )
                        self?.view.window?.rootViewController = mapViewController
                        self?.view.window?.makeKeyAndVisible()
                    }
                }
            }
        }
    }
    
    func showError(_ message: String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
}
