//
//  AddPinViewController.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/31/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import UIKit
import CoreLocation

protocol AddPinViewControllerDelegate: class {
    func didAddPinDetails(_ pin: PinAnnotation, at coordinates: CLLocationCoordinate2D)
    func didCancel()
}
   
class AddPinViewController: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var streetNametextField: UITextField!
    @IBOutlet weak var detailsTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var AddButton: UIButton!
    
    weak var delegate: AddPinViewControllerDelegate?
    
    private let dataService: DataService = FirebaseService()
    
    var imagePicker: ImagePicker!
    var coordinates: CLLocationCoordinate2D?
    var streetName: String?
    var context: Context = .none

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpElements()
        streetNametextField.text = self.streetName
        
        self.navigationItem.title = "Pin Details"
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    func setUpElements(){
        
        Utilities.styleTextFiled(titleTextField)
        Utilities.styleTextFiled(streetNametextField)
        Utilities.styleTextFiled(detailsTextField)
        Utilities.styleFilledButton(AddButton)
        
        titleTextField.text = nil
        detailsTextField.text = nil
    }
    
    @IBAction func onAddPin(_ sender: UIButton) {
        
        if let title = self.titleTextField.text,
            let street = self.streetName,
            let details = self.detailsTextField.text,
            let coordinates = self.coordinates {
            
            if title == "" {
                showAlert(title: "Error", message: "Please fill the required fields")
                return
            }
            let pin = Pin(title: title,
                          streetName: street,
                          details: details,
                          images: [Image(image: self.imageView.image)],
                          type: self.context,
                          user: UserManager.shared.currentUser!,
                          latitude: coordinates.latitude,
                          longitude: coordinates.longitude)
            
            addEvent(pin: pin ) { (success) in
                    if success {
                        self.dismiss(animated: true, completion: nil)
                    }else {
                        self.showAlert(title: "Error", message: "Event saving failed due to unknown error.")
                    }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.delegate?.didCancel()
    }
    
    @IBAction func actionOnAddImage(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    
    private func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
       alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (handler) in
           alert.dismiss(animated: true, completion: nil)
       }))
       return self.present(alert, animated: true, completion: nil)
    }
    
    private func addEvent(pin: Pin, completion: @escaping (Bool) -> ()) {
        
        if let image = pin.images.first, let uiimage = image.image {
            //upload event with image: imageURL
            //first we need to uploadImage
            dataService.uploadImage(image: uiimage) { (imageUrl, error) in
                if error != nil {
                    self.showAlert(title: "Error", message: "Error uploading image!")
                    return
                }
                
                var newPin = pin
                newPin.images = [Image(imageUrl: imageUrl)]
                
                self.dataService.saveEvent(pin: newPin) { (error) in
                    if error != nil{
                        completion(false)
                        return
                    }
                    completion(true)
                    
                }
            }
        }else {
            
            dataService.saveEvent(pin: pin) { (error) in
                if error != nil{
                    completion(false)
                    return
                }
                completion(true)
            }
        }
    }
    
}

extension AddPinViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        self.imageView.image = image
    }
}
