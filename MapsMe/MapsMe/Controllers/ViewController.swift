//
//  ViewController.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/12/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    
    var videoPlayer: AVPlayer?
    
    @IBOutlet weak var videoContentView: UIView!
    var videoPlayerLayer: AVPlayerLayer?

    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setUpElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Set up video in the background
        setUpVideo()
    }
    
    func setUpElements() {
        
        Utilities.styleFilledButton(signUpButton)
        Utilities.styleHollowButton(loginButton)
    }
    
    func setUpVideo(){
        
        //Get the path to the resource in the bundle
        let bundlePath = Bundle.main.path(forResource: "16506478-preview", ofType: "mp4")
        
        guard bundlePath != nil else{
            return
        }
        
        //Create a URL from it
        let url = URL(fileURLWithPath: bundlePath!)
        
        //Create the video player item
        let item = AVPlayerItem(url: url)
        
        //Create the player
        videoPlayer = AVPlayer(playerItem: item)
        
        //Create the layer
        videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
        
        //Adjust the size and frame
        videoPlayerLayer?.frame = CGRect(x: -self.view.frame.size.width*1.5,
                                         y: 0,
                                         width: self.view.frame.width*4,
                                         height: self.view.frame.size.height)
        
        videoContentView.layer.insertSublayer(videoPlayerLayer!, at: 0)
        
        videoContentView.alpha = 0.3
        
        view.backgroundColor = #colorLiteral(red: 0.1298192669, green: 0.1290674366, blue: 0.1416774611, alpha: 1)
        //Add it to the view and play it
        videoPlayer?.playImmediately(atRate: 0.5)
    }


}

