//
//  PinDetailsViewController.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/28/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import UIKit

class PinDetailsViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var streetNameTextField: UITextField!
    
    @IBOutlet weak var detailsLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    var pin: Pin?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpElements()
        
         self.navigationItem.title = "Details"
        
    }
    
    func setUpElements() {
        
        Utilities.styleTextFiled(titleTextField)
        Utilities.styleTextFiled(streetNameTextField)
        
        
        self.titleTextField.text = pin?.title
        self.streetNameTextField.text = pin?.streetName
        self.detailsLabel.text = pin?.details
        
//        self.imageView.image = pin?.attachments.first?.image
        //TODO:
        
    }

    

}
