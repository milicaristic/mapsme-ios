//
//  MapViewController.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/25/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import UIKit
import Foundation
import MapKit

enum Context {
    case attack
    case theft
    case none
}

class MapViewController : UIViewController, LocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    private let dataService: DataService = FirebaseService()
    
    var locations = [PinAnnotation]()
    var userCurrentLocation: CLLocation?
    var arraySelect = ["Attack", "Theft"]
    
    private var selectedContext: Context = .none
    private var isPresenting = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = LocationManager.shared
        LocationManager.shared.delegate = self
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(addAnnotation(longGesture:)))
        self.mapView.addGestureRecognizer(longGesture)

        self.navigationItem.title = "MapMe"
       
        mapView.delegate = self
        mapView.userTrackingMode = .follow
        mapView.showsUserLocation = true
        mapView.register(PinAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataService.getPins { (pins, error) in
            if error != nil {
                //show error
                return
            }
            
            self.mapView.removeAnnotations(self.mapView.annotations)
            
            if let pinsArray = pins {
                for pin in pinsArray {
                    self.addAnnotation(pin: pin)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pinDetails" {
            let vc = segue.destination as? PinDetailsViewController
            guard let pin = sender as? Pin else { return }
            vc?.pin = pin
        }else if segue.identifier == "addPinSegue" {
            let viewController = segue.destination as? AddPinViewController
            self.isPresenting = true
            if let data = sender as? [String: Any] {
                viewController?.coordinates = data["coordinates"] as? CLLocationCoordinate2D
                viewController?.streetName = data["name"] as? String
                viewController?.context = data["context"] as? Context ?? .none
                viewController?.delegate = self
            }
        }
    }
    
    func didFindLocation(_ location: CLLocation) {
        self.zoomTo(location)
        self.userCurrentLocation = location
    }
    
    //TODO:
    private func zoomTo(_ location: CLLocation) {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        DispatchQueue.main.async {
            self.mapView.setRegion(region, animated: true)
            let annotation = MKPointAnnotation()
            annotation.coordinate = location.coordinate
            self.mapView.addAnnotation(annotation)
        }
    }
    
    @objc
    func addAnnotation(longGesture: UIGestureRecognizer) {
        if self.selectedContext != .none {
            let touchPoint = longGesture.location(in: mapView)
            let newCoordinates = mapView.convert(touchPoint, toCoordinateFrom: mapView)
            
            self.geocode(newCoordinates) { (name, error) in
                let dictionary = ["coordinates" : newCoordinates,
                                  "name" : name ?? "Unknown steet name",
                                  "context" : self.selectedContext] as [String : Any]
                
                if !self.isPresenting {
                    self.performSegue(withIdentifier: "addPinSegue", sender: dictionary)
                }
            }
        }else {
            let alert = UIAlertController(title: "Error", message: "Please select event type!", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (handler) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func addAnnotation(pin: Pin) {
        
        let annotation = PinAnnotation(pin: pin)
        
        if !(mapView.annotations.contains(where: {
            let existingAnnotation = $0 as? PinAnnotation
            return existingAnnotation?.pin.title == annotation.pin.title
        })) {
            mapView.addAnnotation(annotation)
        }
        
        mapView.reloadInputViews()
    }
    
    private func geocode(_ location: CLLocationCoordinate2D, completion: @escaping(String?, Error?) -> Void) {
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(CLLocation(latitude: location.latitude, longitude: location.longitude)) { placemarks, error in
            
            if let e = error {
                completion(nil, e)
            } else {
                
                let placeArray = placemarks
                var placeMark: CLPlacemark!
                placeMark = placeArray?[0]
                completion(placeMark.name, nil)
            }
        }
    }
    

    @IBAction func actionOnAdd(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let closure = { (action: UIAlertAction!) -> Void in
            self.selectedContext = action.title == "Attack" ?  .attack : .theft
        }
        
        for field in arraySelect {
            alert.addAction(UIAlertAction(title: field, style: .default, handler: closure))
        }
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: {(_) in }))
        
        self.present(alert, animated: false, completion: nil)
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = PinAnnotationView(annotation: annotation, reuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        guard let customAnnotation = annotation as? PinAnnotation else { return nil }
        
        annotationView.pin = customAnnotation.pin
        annotationView.canShowCallout = true
        annotationView.isEnabled = true
        annotationView.tintColor = #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1)
        
        let btn = UIButton(type: .detailDisclosure)
        annotationView.rightCalloutAccessoryView = btn
        
        
        if annotation.coordinate.latitude == self.userCurrentLocation?.coordinate.latitude {
            annotationView.canShowCallout = false
            annotationView.image = #imageLiteral(resourceName: "userPin")
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        guard let view = view as? PinAnnotationView else { return }
        self.performSegue(withIdentifier: "pinDetails", sender: view.pin)
    }
}

extension MapViewController: AddPinViewControllerDelegate {
    
    func didAddPinDetails(_ pin: PinAnnotation, at coordinates: CLLocationCoordinate2D) {
//        self.addAnnotation(annotation: pin, at: coordinates)
    }
    
    func didCancel() {
        self.isPresenting = false
    }
    
}
