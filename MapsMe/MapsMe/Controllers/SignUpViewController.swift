//
//  SingUpViewController.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/15/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var singUpButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    private let dataService: DataService = FirebaseService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpElements()
        
        navigationController?.navigationItem.setHidesBackButton(false, animated: true)
    }
    
    func setUpElements() {
        
        errorLabel.alpha = 0
        
        Utilities.styleTextFiled(firstNameTextField)
        Utilities.styleTextFiled(lastNameTextField)
        Utilities.styleTextFiled(emailTextField)
        Utilities.styleTextFiled(passwordTextField)
        Utilities.styleFilledButton(singUpButton)
        
    }
    
    func validateFields() -> String? {
        
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            return "Please fill in all fields."
        }
        //Check password is valid from the utilities
        
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        //TODO:
        //show this message!
        if Utilities.isPasswordValid(password: cleanedPassword) == false {
            //Password isn't secure enough
            
            return "Please make sure your password is at least 8 characters, contains a special character and a number."
        }
        
        return nil
    }
    
    
    @IBAction func singUpTapped(_ sender: Any) {
        
        //Validate the fields
        let error = validateFields()
        
        if error != nil {
            self.showError(error!)
        }
        else{
            
            //Create cleandes versions od the data
            let firstName = firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName = lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            dataService.register(firstName: firstName, lastName: lastName, email: email, password: password) { (success) in
                if (!success){
                    self.showError("Error with registration")
                }else{
                    self.dataService.getUser { (user, error) in
                        if error != nil {
                            //show error
                            return
                        }
                        UserManager.shared.currentUser = user
                        DispatchQueue.main.async {
                            self.transitionToMap()
                        }
                    }
                }
            }
        }
    }
    
    func showError(_ message: String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    func transitionToMap() {
        
        let mapViewController = storyboard?.instantiateViewController(withIdentifier: Constants.Storybord.map ) as? MapViewController
        view.window?.rootViewController = mapViewController
        view.window?.makeKeyAndVisible()
    }
}
