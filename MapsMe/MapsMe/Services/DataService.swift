//
//  DataService.swift
//  MapsMe
//
//  Created by Milica Ristic on 14.4.21..
//  Copyright © 2021 Milica Ristic. All rights reserved.
//

import Foundation
import UIKit

protocol DataService {
    
    func getUser(completion: @escaping (User?, Error?) -> ())
    
    func getPins(completion: @escaping ([Pin]?, Error?) -> ())
    
    func saveEvent(pin: Pin, completion: @escaping (Error?) -> ())
    
    func uploadImage(image: UIImage, completion: @escaping (String?, Error?) -> ())
    
    func register(firstName: String, lastName: String, email: String, password: String, completion: @escaping (Bool) -> ())
    
    func updateUser(userId: String, firstName: String, lastName: String, email: String, password: String, completion: @escaping (Bool) -> ())
    
    func login(email: String, password: String, completion: @escaping (Error?) -> ())
    


}
