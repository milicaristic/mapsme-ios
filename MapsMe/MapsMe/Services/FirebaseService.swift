//
//  FirebaseService.swift
//  MapsMe
//
//  Created by Milica Ristic on 14.4.21..
//  Copyright © 2021 Milica Ristic. All rights reserved.
//

import Foundation
import Firebase
import FirebaseCore
import FirebaseFirestore
import UIKit
import MapKit

class FirebaseService : DataService {
    
    var db: Firestore = Firestore.firestore()
    var ref: DatabaseReference! = Database.database().reference()
    
    func getUser(completion: @escaping (User?, Error?) -> ()) {
        
        if let userId = UserDefaults.standard.value(forKey: "userId") as? String {
            
            let docRef = db.collection("users").document(userId)
            
            docRef.getDocument { (document, error) in
                if let document = document, document.exists {
                    
                    guard let dictionary = document.data() else {
                        completion(nil, nil)
                        return
                    }
                    
                    let user = User(with: dictionary)
                    
                    completion(user, nil)
                } else {
                    print("Document does not exist")
                    completion(nil, error)
                }
            }
            
        }else {
            completion(nil, nil)
        }
    }
    
    func getPins(completion: @escaping ([Pin]?, Error?) -> ()) {
        
        db.collection("events").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(nil, err)
            } else {
                
                var pinsArray = [Pin]()
                
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    
                    let dictionary = document.data()
                    let pin = Pin(with: dictionary)
                    pinsArray.append(pin)
                }
                
                completion(pinsArray, nil)
            }
        }
    }
    
    func saveEvent(pin: Pin, completion: @escaping (Error?) -> ()){

        let data: [String : Any] = [
            "title": pin.title,
            "streetName": pin.streetName,
            "details": pin.details,
            "image": pin.images.first?.imageUrl ?? "",
            "type" : pin.type == .attack ? "attack" : "theft",
            "user" : UserManager.shared.currentUser?.toDictionary() ?? [:],
            "latitude": pin.latitude,
            "longitude": pin.longitude
        ]
        
        db.collection("events").addDocument(data: data) { err in
            if let err = err {
                print("Error writing document: \(err)")
                completion(err)
            } else {
                print("Document successfully written!")
                completion(nil)
            }
        }
    }
    func uploadImage(image: UIImage, completion: @escaping (String?, Error?) -> ()){
        
        guard let imageData: Data = image.jpegData(compressionQuality: 0.7) else {
            return
        }

        let metaDataConfig = StorageMetadata()
        metaDataConfig.contentType = "image/jpg"

        let filePath = UUID().uuidString
        
        let storageRef = Storage.storage().reference(withPath: filePath)

        storageRef.putData(imageData, metadata: metaDataConfig){ (metaData, error) in
            if let error = error {
                print(error.localizedDescription)

                completion(nil, error)
            }

            storageRef.downloadURL(completion: { (url: URL?, error: Error?) in
                if let url = url?.absoluteString {
                    completion(url, nil)
                }else {
                    completion(nil, error)
                }
            })
        }
    }
    func register(firstName: String, lastName: String, email: String, password: String, completion: @escaping (Bool) -> ()){
        
        //Create the user with createUser - Firebase func
        Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
            
            // Check for errors
            if err != nil {
                completion(false)
            }
            else {
 
                let userId = result?.user.uid
                UserDefaults.standard.set(userId, forKey: "userId")
                
                self.updateUser(userId: userId! , firstName: firstName, lastName: lastName, email: email, password: password) { (success) in
                    completion(success)
                }
            }
        }
    }
    
    func updateUser(userId: String, firstName: String, lastName: String, email: String, password: String, completion: @escaping (Bool) -> ()) {
        
        var data = [String:Any]()
        
        data["uid"] = userId
        data["firstName"] = firstName
        data["lastName"] = lastName
        data["email"] = email
        data["password"] = password
        
        
        db.collection("users").document(userId).setData(data) { (error) in
            completion(error == nil)
        }
    }
    
    func login(email: String, password: String, completion: @escaping (Error?) -> ()){
        
        //Signing in the user
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            let userId = result?.user.uid
            UserDefaults.standard.set(userId, forKey: "userId")
            completion(error)
        }
    }
}
