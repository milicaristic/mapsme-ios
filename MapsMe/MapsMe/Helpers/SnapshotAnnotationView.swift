//
//  PinAnnotationView.swift
//  MapsMe
//
//  Created by Milica Ristic on 9/8/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class PinAnnotationView: MKAnnotationView {
    
    override var annotation: MKAnnotation? { didSet { configureDetailView(with: self.pin) } }
    
    var pin: Pin?
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
}

private extension PinAnnotationView {
    func configure() {
        canShowCallout = true
        configureDetailView(with: self.pin)
    }
    
    func configureDetailView(with pin: Pin?) {
        guard let annotation = annotation else { return }
        
        let rect = CGRect(origin: .zero, size: CGSize(width: 200, height: 100))
        
        
        
        let imageView = UIImageView(frame: rect)
//        imageView.image = pin?.attachments.first?.image
        
        //TODO:
        
        if pin?.type == .theft {
             self.set(image: UIImage.init(named: "bag")!, with: #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1))

        }else if pin?.type == .attack {
            self.set(image: UIImage.init(named: "bag")!, with: #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1))
            
        }else {
            self.set(image: #imageLiteral(resourceName: "userPin"), with: #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1))
            
        }

        let options = MKMapSnapshotter.Options()
        options.size = rect.size
        options.mapType = .satelliteFlyover
        options.camera = MKMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 250, pitch: 65, heading: 0)
        
        detailCalloutAccessoryView = imageView
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: rect.width),
            imageView.heightAnchor.constraint(equalToConstant: rect.height)
            ])
    }
}

extension UIImage {
    
    func colorized(color : UIColor) -> UIImage {
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        if let context = UIGraphicsGetCurrentContext() {
            context.setBlendMode(.multiply)
            context.translateBy(x: 0, y: self.size.height)
            context.scaleBy(x: 1.0, y: -1.0)
            context.draw(self.cgImage!, in: rect)
            context.clip(to: rect, mask: self.cgImage!)
            context.setFillColor(color.cgColor)
            context.fill(rect)
        }
        
        let colorizedImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return colorizedImage!
        
    }
}

extension MKAnnotationView {
    
    public func set(image: UIImage, with color : UIColor) {
        let view = UIImageView(image: image.withRenderingMode(.alwaysTemplate))
        view.tintColor = color
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        guard let graphicsContext = UIGraphicsGetCurrentContext() else { return }
        view.layer.render(in: graphicsContext)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.image = image
    }
    
}
