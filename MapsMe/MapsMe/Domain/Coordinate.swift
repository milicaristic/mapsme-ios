//
//  Location.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/13/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import Foundation

struct Coordinate{
    let latitude: Double
    let longitude: Double
}

