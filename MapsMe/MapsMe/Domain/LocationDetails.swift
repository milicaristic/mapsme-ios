//
//  Description.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/13/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import Foundation
import UIKit

struct LocationDetails {
    let image: UIImage
    let text: String
    let time: Date
    
}
