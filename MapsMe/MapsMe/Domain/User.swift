//
//  User.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/13/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import Foundation

struct User {
    let id: String?
    let firstName: String?
    let lastName: String?
    let email: String?
    let password: String?
    
    init(id: String? = nil, firstName: String? = nil, lastName: String? = nil, email: String? = nil, password: String? = nil) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.id = id
        self.password = password
    }
    
    init(with dictionary: [String: Any]) {
        let firstName = dictionary["firstname"] as? String ?? ""
        let lastName = dictionary["lastname"] as? String ?? ""
        let email = dictionary["email"] as? String ?? ""
        let id = dictionary["uid"] as? String ?? ""
        let password = dictionary["password"] as? String ?? ""
        
        
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.id = id
        self.password = password
    }
    
    func toDictionary() -> [String: Any] {
        var dictionary = [String:Any]()
        
        dictionary["firstName"] = self.firstName
        dictionary["lastname"] = self.lastName
        dictionary["email"] = self.email
        dictionary["uid"] = self.id

        return dictionary
    }
}

