//
//  Image.swift
//  MapsMe
//
//  Created by Milica Ristic on 20.4.21..
//  Copyright © 2021 Milica Ristic. All rights reserved.
//

import UIKit

struct Image {
    let imageUrl: String?
    var image: UIImage?
    
    init(imageUrl: String? = nil, image: UIImage? = nil) {
        self.imageUrl = imageUrl
        self.image = image
    }
}
