//
//  Pin.swift
//  MapsMe
//
//  Created by Milica Ristic on 8/13/20.
//  Copyright © 2020 Milica Ristic. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class PinAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var pin: Pin
    
    init(pin: Pin) {
        let coordinate = CLLocationCoordinate2D(latitude: pin.latitude, longitude: pin.longitude)
        self.coordinate = coordinate
        self.pin = pin
        super.init()
    }
}


struct Pin {
    let title: String
    let streetName: String
    let details: String
    var images: [Image]
    let type: Context
    var user: User? = nil
    let latitude: Double
    let longitude: Double
    
   
    
    init(title: String? = nil, streetName: String? = nil, details: String, images: [Image], type: Context, user: User, latitude: Double? = 0, longitude: Double? = 0) {
        
        self.title = title!
        self.streetName = streetName!
        self.details = details
        self.images = images
        self.type = type
        self.user = user
        self.latitude = latitude!
        self.longitude = longitude!
    }

    init(with dictionary: [String: Any]) {
        let title = dictionary["title"] as? String ?? ""
        let details = dictionary["details"] as? String ?? ""
        let type = dictionary["type"] as? String ?? ""
        let streetName = dictionary["streetName"] as? String ?? ""
        let imageUrl = dictionary["image"] as? String ?? ""
        let latitude = dictionary["latitude"] as? Double ?? 0
        let longitude = dictionary["longitude"] as? Double ?? 0
        
        if let userDict = dictionary["user"] as? [String:Any] {
            let user = User(with: userDict)
            self.user = user
        }
        
        self.title = title
        self.streetName = streetName
        self.details = details
        self.images = [Image(imageUrl: imageUrl)]
        self.type = type == "attack" ? .attack : .theft
        self.latitude = latitude
        self.longitude = longitude
    }

}
